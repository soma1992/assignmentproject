import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import AsyncStorage from '@react-native-community/async-storage';
import { persistStore, persistReducer } from "redux-persist";
import { createLogger } from "redux-logger";
import reducers from "../App/Reducers";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  // blacklist: ["form"]
};

const persistedReducer = persistReducer(persistConfig, reducers);
const middlerwares = [ReduxThunk];
const store = createStore(
  persistedReducer,
  {},
  applyMiddleware(...middlerwares),
);
if (__DEV__) {
      middlerwares.push(createLogger());
     }
export default store;
export const persistor = persistStore(store);
// export default (rootReducer) => {
//   const persistedReducer = persistReducer(persistConfig, rootReducer);
//   const middlerwares = [ReduxThunk];
//   if (__DEV__) {
//     middlerwares.push(createLogger());
//   }
//   const store = createStore(
//     persistedReducer,
//     {},
//     applyMiddleware(...middlerwares)
//   );
//   const persistor = persistStore(store);
//   return { store, persistor }
// }
