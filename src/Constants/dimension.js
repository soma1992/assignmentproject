import { Platform } from 'react-native';
import { RFValue } from "./textSizeFormatter";

const fontBold = Platform.OS == "ios" ? "Nunito-Bold" : "nunito_bold"
const fontRegular = Platform.OS == "ios" ? "Nunito-Regular" : "nunito_regular"

const SIZE_MARGIN = {
    SMALL: 5,
    MEDIUM: 10,
    LARGE: 15,
    XLARGE: 20,
    ACTION_BAR: 55,
    ICON: 22,
};

const SIZE_TEXT = {
    SMALL: RFValue(9),
    MEDIUM: RFValue(12),
    LARGE: 17,
    XLARGE: 24,
};

const FONT_FAMILY = {
    BOLD: fontBold,
    ITALIC: 14,
    REGULAR: fontRegular,
};

export { SIZE_TEXT, SIZE_MARGIN ,FONT_FAMILY}