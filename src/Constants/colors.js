const COLORS = {
    //Theame Colors change
    
    PRIMARY: '#E66957',
    SECONDARY: '#FFEBDA',
    ACTION: '#19C5B9',
    PRIMARY_TEXT: '#454545',
    WHITE: '#FFF',
    BLACK: '#000',
    GRAY: '#909091',
    LIGHT_GRAY: '#e6e6e6',
}

export {COLORS};