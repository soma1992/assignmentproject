import { StyleSheet, Dimensions } from 'react-native';
import {COLORS} from '../Constants/colors';
import {RFValue} from '../Constants/textSizeFormatter'
export const homeStyle = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: COLORS.WHITE,
        marginTop: '10%',
        
      },
      formContainer:{
        margin:'5%'
      },
      headerContainer: {
        height: 80
      },
      inputStyle: {
        backgroundColor: COLORS.WHITE,
        borderWidth: 1,
        borderColor: COLORS.LIGHT_GRAY,
        borderRadius: 5,
        fontSize: RFValue(14),
        lineHeight: 23,
        alignItems: 'center',
        padding: 30
    },
    imgView: {
      height: 60,
      width: 60
  },
  textStyle: {
    color: COLORS.GENDER,
    fontFamily: "Nunito",
    justifyContent: 'center'
},
btnTrans: {
  marginTop: 10,
  marginBottom: 10,
  height: 45
},
buttonView:
{
    width:'100%',position:'absolute',bottom:10
},
buttonStyle: {
  textAlign: 'center',
  borderRadius: 5,
  height: 55,
  marginLeft:20,marginRight:20
},
topicView: {
  //  margin:15,
  marginBottom: '1%',
  //  padding:15,
  borderRadius: 50,
},
imgFull: {
  width: "100%",
  height: 200,
},
mapView:{
  height:50,
  width:'100%'
},
map:{
  flex:1
}

});