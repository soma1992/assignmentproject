import I18n from "react-native-i18n";
import en from "./en.json";

I18n.fallbacks = true;
I18n.translations = {
  en
};
//const currentLocale = I18n.currentLocale();

// Is it a RTL language?
//export const isRTL = currentLocale.indexOf('he') === 0 || currentLocale.indexOf('ar') === 0;

// Allow RTL alignment in RTL languages
//ReactNative.I18nManager.allowRTL(isRTL);

// The method we'll use instead of a regular string
export function strings(name, params = {}) {
  return I18n.t(name, params);
}

// this methid for chnaging langaue inside button call
// export const switchLanguage = (lang, component) => {
//                     I18n.locale = lang;
// component.forceUpdate();
// };

export default I18n;
