import { createStackNavigator, createBottomTabNavigator, createDrawerNavigator, createSwitchNavigator } from 'react-navigation-stack';
import { Platform, Image, View, Dimensions, StyleSheet ,Text} from "react-native";
import React from 'react';
import Home from '../Containers/Home';
import EmployeeName from '../Containers/EmployeeName';
import EmployeeDetails from '../Containers/EmployeeDetails';
    const AppNavigation = createStackNavigator({

      Home: { screen: Home },
      EmployeeName:{screen:EmployeeName},
      EmployeeDetails:{screen:EmployeeDetails}
    }, {
      defaultNavigationOptions: ({ navigation }) => ({
        headerMode: "none",
        headerTitle: "ADD Employee Details",
        headerTransparent: Platform.OS == "ios" ? false : true,
      })
    })
    
export default AppNavigation;