import React from 'react';
import { View, Text ,Platform} from 'react-native';
import { COLORS } from '../../Constants/colors';
import { RFValue } from '../../Constants/textSizeFormatter';

const fontRegular = Platform.OS == "ios" ? "Nunito-Regular" : "nunito_regular"
const fontBold = Platform.OS == "ios" ? "Nunito-Bold" : "nunito_bold"
const fontNunito = Platform.OS == "ios" ? "Nunito" : "nunito"

const ActionHolder = (props) => {

  return (
    <View style={styles.divMargin} >
      <View >
        <Text style={styles.textStyle}>{props.placeholder}</Text>
      </View>
      {props.children}
    </View>
  );
};

const styles = {
  containerStyle: {
    borderRadius: 3,
    height: 20,
    marginLeft: 20,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: -10,
  },
  textStyle: {
    fontFamily: fontNunito,
    fontSize: RFValue(14),
    color: COLORS.GENDER,
    paddingRight: 5,
    marginBottom: 5

  },
  divMargin: {
    marginTop: 5,
    marginBottom: 5,
  },
};

export { ActionHolder };