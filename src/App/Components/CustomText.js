import React from "react";
import { Text, PixelRatio, Platform } from "react-native";
import {  RFValue} from "../../Constants/textSizeFormatter";
import { COLORS } from "../../Constants/colors";

const fontRegular = Platform.OS == "ios" ? "Nunito-Regular" : "nunito_regular"


const CustomText = ({
  label,
  isTitleFont,
  color,
  customStyles,
  lineNumbers,
  fitToText
}) => {
  return (
    <Text
      adjustsFontSizeToFit={fitToText}
      allowFontScaling={false}
      numberOfLines={lineNumbers}
      style={
        isTitleFont
          ? [styles.withTextBold, color,customStyles]
          : [styles.labelStyle, color, customStyles]
      }
    >
      {label}
    </Text>
  );
};

const styles = {
  labelStyle: {
    fontSize: RFValue(13),
    fontFamily: fontRegular,
  },

  withTextBold: {
    fontSize: RFValue(18),
    color: COLORS.PRIMARY_TEXT,
    fontWeight: "600",
    fontFamily: fontRegular
  }
};

export { CustomText };