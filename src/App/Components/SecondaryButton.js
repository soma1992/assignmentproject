import React from 'react';
import { Text, TouchableOpacity, Platform, PixelRatio } from 'react-native';
import { COLORS } from '../../Constants/colors';
import { RFValue } from '../../Constants/textSizeFormatter';



const SecondaryButton = ({ label, bgColor, txtColor, onPress, borderCol, customStyle, isDisabled = false, disableColor }) => {

    buttonStyle.customStyle = customStyle

        return (
            <TouchableOpacity onPress={onPress} style={[buttonStyle.buttonBackStyle, { backgroundColor: bgColor }, { borderColor: borderCol }, buttonStyle.customStyle]}>
                <Text style={[buttonStyle.textStyle, { color: txtColor }]}>
                    {label}
                </Text>
            </TouchableOpacity >
        );
    
};

const buttonStyle = {
    buttonBackStyle: {
        height: 55,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: COLORS.ACTION,

    },
    textStyle: {
        width: '90%',
        textAlignVertical: 'center',
        textAlign: 'center',
        fontSize: RFValue(16),
        fontWeight: 'bold'
    },
    customStyle: {

    },
};

export { SecondaryButton } 