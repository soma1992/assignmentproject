import React from "react";
import {
    Modal,
    View,
    Dimensions,
    SafeAreaView,
    KeyboardAvoidingView,
    Platform
} from "react-native";
import { COLORS } from "../../Constants/colors";

const CustomModal = props => {

    let backGroundCol = (props.backColor) ? props.backColor : Platform.OS == 'ios'?COLORS.SHADOW_COLOR :'#ffb3'

    return (
        <SafeAreaView>
            <Modal
                animationType={"fade"}
                transparent={props.transparent}
                visible={props.isVisible}
                presentationStyle="overFullScreen"
                onRequestClose={props.onRequestClose}

            >
                <View
                    style={{
                        flex: 1,
                        alignItems: "center",
                        backgroundColor: backGroundCol,
                        shadowColor: "#000000",
                        shadowOpacity: 0.8,
                        shadowRadius: 2,
                        shadowOffset: {
                            height: 1,
                            width: 1
                        },
                        justifyContent: "flex-end"
                    }}
                >
                    <KeyboardAvoidingView
                        keyboardShouldPersistTaps="handled"
                        behavior="padding"
                        enabled={Platform.select({
                            ios: () => true,
                            android: () => false
                        })()}
                    >

                        <View
                            style={{
                                backgroundColor: Platform.OS == 'ios' ?"#ffffff":'#fff',
                                paddingLeft: 20,
                                paddingTop: 24,
                                paddingBottom: 28,
                                paddingRight: 20,
                                width: Dimensions.get("window").width
                            }}
                        >
                            {props.children}
                        </View>
                    </KeyboardAvoidingView>
                </View>
            </Modal>
        </SafeAreaView>
    );
};

export { CustomModal };
