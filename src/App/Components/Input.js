import React from 'react';
import { TextInput, View, Text, PixelRatio } from 'react-native';
import {COLORS} from '../../Constants/colors';
import { RFValue } from '../../Constants/textSizeFormatter';

const Input = ({ 
  label,
  customHeight,
  onEndEditing, 
  placeholder,
  editable, 
  style,
  secureTextEntry, 
  returnKeyLabel, 
  placeholderIcon, 
  inputValue,
  meta: { touched, error }, 
  input: { value, onChange },
  multiline,
  maxLength,
  autoCapitalize,
  keyboardType, returnKeyType
 }) => {
  const { inputStyle, labelStyle, containerStyle, errorText, iconContainerStyles} = styles;

  return (
    <View style={containerStyle}>
      <TextInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        placeholderTextColor={"gray"}
        autoCorrect={false}
        style={inputStyle}
        multiline = {multiline ? multiline : false}
        numberOfLines = {multiline ? 4 : 1}
        textAlignVertical={multiline?'top':'auto'}
        onChangeText={(value) => onChange(value)}
        value={(value)?value:inputValue}
        onEndEditing={onEndEditing}
        keyboardType={keyboardType}
        autoCapitalize={ autoCapitalize ? autoCapitalize:"none"}
        returnKeyType={returnKeyType}
        returnKeyLabel={returnKeyLabel}
        editable={editable}
        height={customHeight?customHeight:50}
        maxLength={maxLength}
         />
      <View style={iconContainerStyles}>{placeholderIcon}</View>
      {touched && error && <Text style={errorText}>{error}</Text>}
    </View>
  );
};

const styles = {
  inputStyle: {
    backgroundColor: COLORS.WHITE,
    borderWidth:1,
    borderColor:COLORS.LIGHT_GRAY,
    borderRadius: 5,
    fontSize: RFValue(14),
    paddingLeft: 10,
    paddingRight: 10,
    lineHeight: 23,
    paddingTop: 10,
    paddingBottom:10,
  },
  labelStyle: {
    fontSize: RFValue(14),
  },
  containerStyle: {
    flexDirection: 'column',
  },
  iconContainerStyles: {
    position: "absolute",
    right: 0,
    alignItems: "center",
    justifyContent: "center",
    bottom: 0,
    padding: 13
  },
  errorText: {
    color: "red"
  }
};

export { Input };