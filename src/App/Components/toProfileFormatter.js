import ImagePicker  from 'react-native-image-crop-picker';

const compImageQuality = 0.6;

const openImagePickerRect = (cropit, circular=false) =>{

    return new Promise((resolve, reject) => {
        ImagePicker.openPicker({
            width: 500,
            height: 500,
            cropping: true,
            cropperCircleOverlay: circular,
            //  compressImageMaxWidth: 1000,
            // compressImageMaxHeight: 1000,
            compressImageQuality: compImageQuality,
            // compressVideoPreset: 'MediumQuality',
            includeExif: true,
          }).then(image => {
                resolve({isResolved:true, data:image});
          }).catch(err=> {
                reject({isResolved:false, data:null});
          })

    })
  
  
  
}

const openImageCameraRect = (cropit, circular=false) =>{

      return new Promise((resolve, reject) => {
          ImagePicker.openCamera({
                width: 500,
                height: 500,
              //   cropping: true,
                cropperCircleOverlay: circular,
                // compressImageMaxWidth: 1000,
                // compressImageMaxHeight: 1000,
                compressImageQuality: compImageQuality,
                includeExif: true,
            }).then(image => {
                  resolve({isResolved:true, data:image});
            }).catch(err=> {
                  reject({isResolved:false, data:null});
            })
  
      })
    
    
    
  }

  export {openImagePickerRect,openImageCameraRect }
