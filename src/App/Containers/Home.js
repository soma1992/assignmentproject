import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Platform,
  ImageBackground
} from "react-native";
import Geolocation from '@react-native-community/geolocation';
import MapView , {Marker} from 'react-native-maps';
import { strings } from "../../locales/i18n.js";
import { homeStyle } from "../../styles/homeStyle";
import { COLORS } from '../../Constants/colors';
import { SIZE_TEXT } from "../../Constants/dimension";
import { Field, reduxForm } from "redux-form";
import validator from "validator";
import { openImagePickerRect, openImageCameraRect } from '../Components/toProfileFormatter';
import { employeeEmailChanged, employeeUsernameChanged ,registerDataClear,registerPhoneChanged,submitEmployeeDetails} from '../Actions/homeActions'
import {ActionHolder, Input,CustomText,CustomModal,SecondaryButton} from '../Components';
import { connect } from 'react-redux';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';

const PHONE_REGEX = new RegExp("^([0-9]{10})$|^([0-9]{10})$");

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openChoosePicModal:false,
      avatarSource:'',
      imageData: '',
      latitude: 0,
      lognitude: 0,

    };
  }

  componentWillMount() {
    this.props.registerDataClear();
    Geolocation.getCurrentPosition(
      position => {
        const latitude = JSON.stringify(position);
        this.setState({latitude});
      },
      error => Alert.alert('Error', JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    );
    this.watchID = Geolocation.watchPosition(position => {
      const lognitude = JSON.stringify(position);
      this.setState({lognitude});
    });

}

componentWillUnmount = () => {
  this.watchID != null && Geolocation.clearWatch(this.watchID);
}

  onEmailChange = email => {
    this.props.employeeEmailChanged(email);
  };

  onUsernameChange = name => {
    this.props.employeeUsernameChanged(name);
  };

  onPhoneChange = (number) => {
    this.props.registerPhoneChanged(number);
}

  submitDetails = () => {
  
   if (this.props.email || this.props.username || this.props.reg_phone) {
        let bodyForm = new FormData();
        bodyForm.append('email', this.props.email);
        bodyForm.append('username', this.props.username);
        bodyForm.append('phone', this.props.reg_phone);
        if (this.state.imageData) {
            let filePaths = this.state.imageData.path.split('/');
            let fileName = filePaths[filePaths.length -1].toString();
            bodyForm.append('image', { uri: this.state.imageData.path, name: fileName, type: this.state.imageData.mime });
        }
        else{
          console.log(bodyForm, "bodyform")
        this.props.submitEmployeeDetails(bodyForm);
        }

    }  
}

  openFromGallery = () => {

    try {
        this.setState({ openChoosePicModal: false });
        setTimeout(async () => {
            const imageData = await openImagePickerRect();
            if (imageData.isResolved) {
                this.setState({ avatarSource: { uri: imageData.data.path }, imageData: imageData.data });
            }
        }, 1000);

    } catch (err) {
        this.setState({ openChoosePicModal: false })

    }
}

checkCameraAndroid = () => {
  check(PERMISSIONS.ANDROID.CAMERA)
    .then(result => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          break;
        case RESULTS.DENIED:
          Promise.all([
            request(PERMISSIONS.ANDROID.CAMERA),
          ]).then(([photolibraryStatus]) => {
          });
          break;
        case RESULTS.GRANTED:
          this.openCamera();
          break;
        case RESULTS.BLOCKED:
          break;
      }
    })
    .catch(error => {
      // …
    });
}

checkCameraiOS = () => {
  check(PERMISSIONS.IOS.CAMERA)
    .then(result => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          break;
        case RESULTS.DENIED:
          Promise.all([
            request(PERMISSIONS.IOS.CAMERA)
          ]).then(([cameraStatus]) => {
          });
          break;
        case RESULTS.GRANTED:
          this.openCamera();
          break;
        case RESULTS.BLOCKED:
          break;
      }
    })
    .catch(error => {
      // …
    });
}

openFromCamera = () => {
  if (Platform.OS == 'ios') {
      this.checkCameraiOS();
  } else {
      this.checkCameraAndroid();
  }
}

openCamera = () => {
  console.log('in camera')
  try {
      this.setState({ openChoosePicModal: false });

      setTimeout(async () => {
          const imageData = await openImageCameraRect();
          if (imageData.isResolved) {
              this.setState({ avatarSource: { uri: imageData.data.path }, imageData: imageData.data });
          }
      }, 1000);


  } catch (err) {
      this.setState({ openChoosePicModal: false })
  }
}

  imageSource = () => {
    if (this.state.avatarSource) {
      return (
          <View style={homeStyle.topicView}>
              <ImageBackground style={homeStyle.imgFull} resizeMode='cover' source={(this.state.edit && !this.state.imageData) ? { uri: this.state.avatarSource } : this.state.avatarSource} />
          </View>
      )
  }
  else {
   return(
  <TouchableOpacity onPress={() => this.setState({ openChoosePicModal: true })}>
  <View style={homeStyle.inputStyle}>
    <ImageBackground style={homeStyle.imgView} resizeMode='cover' source={require('../Assets/iconphotos.png')} />
    <View style={{ marginTop: 20 }}>
      <CustomText customStyles={homeStyle.textStyle} label={"ADD Photos"}></CustomText>
    </View>
  </View>
</TouchableOpacity>
   )}
  }

   selectProfilePicModal = () => {

    if (this.state.openChoosePicModal) {
        return (
            <CustomModal transparent={true}>
                <SecondaryButton onPress={() => this.openFromGallery()} label={strings("LOGIN.gallery")} txtColor={COLORS.PRIMARY} bgColor={COLORS.WHITE} borderCol={COLORS.PRIMARY} customStyle={homeStyle.btnTrans}  ></SecondaryButton>
                <SecondaryButton onPress={() => this.openFromCamera()} label={strings("LOGIN.camera")} txtColor={COLORS.PRIMARY} bgColor={COLORS.WHITE} borderCol={COLORS.PRIMARY} customStyle={homeStyle.btnTrans}  ></SecondaryButton>
                <SecondaryButton onPress={() => this.setState({ openChoosePicModal: false })} label={"CLOSE"} txtColor={COLORS.PRIMARY} bgColor={COLORS.WHITE} borderCol={COLORS.WHITE} customStyle={homeStyle.btnTrans}  ></SecondaryButton>
            </CustomModal>
        )
    }

}
  getView (){
    const { handleSubmit } = this.props;
    let myRegion ={
      latitude:12.9075,
      longitude:77.6492,
      latitudeDelta:0.1,
      longitudeDelta:0.1
    }
    let myLocation ={latitude:this.state.latitude , longitude:this.state.longitude}
    return(
      <View style={{flex:1}}>
      <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{
        flexGrow: 1,
        justifyContent: 'space-between'
      }}>
        <View style={homeStyle.mainContainer}>
          <View style={homeStyle.formContainer}>
            <ActionHolder placeholder={strings("LOGIN.email")} bgColor={COLORS.ACTION} >
              <Field
                name="email"
                component={Input}
                label={strings("LOGIN.email")}
                onChange={text => this.onEmailChange(text)}
                value={this.props.email}
                style={{ fontSize: SIZE_TEXT.MEDIUM }}
                autoFocus={true}
                keyboardType={"email-address"}
                returnKeyType={"next"}
              />
            </ActionHolder>
            <ActionHolder placeholder={strings("LOGIN.username")} bgColor={COLORS.ACTION}>
              <View style={{ height: 60, }} >
          <Field
              name="username"
              component={Input}
              label={strings("username")}
              onChange={text => this.onUsernameChange(text)}
              value={this.props.username}
              autoFocus={true}
              autoCapitalize={"sentences"}
              returnKeyType={"next"}
          />

              </View>

            </ActionHolder>
            <ActionHolder placeholder={strings("LOGIN.phone")} bgColor={COLORS.ACTION}>
              <Field
                name="reg_phone"
                component={Input}
                label={strings("phone")}
                onChange={text => this.onPhoneChange(text)}
                value={this.props.reg_phone}
                autoFocus={true}
                keyboardType={"numeric"}
                returnKeyType={"next"}
              />
            </ActionHolder>
            <ActionHolder placeholder={"Add pic"} bgColor={COLORS.SECONDARY}>
                <TouchableOpacity onPress={() => this.setState({ openChoosePicModal: true })}>
                      {this.imageSource()} 
                 </TouchableOpacity> 
             </ActionHolder> 
            
           <View style={homeStyle.mapView}></View>
             <MapView style={homeStyle.map} myRegion={myRegion}>
               <Marker  coordinate= {myLocation}/>

             </MapView>
          </View>
          <View style={homeStyle.buttonView}>
                        <SecondaryButton onPress={handleSubmit(this.submitDetails)} label={"Submit "} txtColor={COLORS.PRIMARY} bgColor={COLORS.WHITE} borderCol={COLORS.PRIMARY} customStyle={homeStyle.buttonStyle} ></SecondaryButton>
                   </View>
        </View>
      </ScrollView>
      </View>
    )
  }
  render() {
    return (
      <View style={{flex:1}}>
        {this.getView()}
        {this.selectProfilePicModal()}
        </View>
    )
  }
}
const validate = values => {
  const errors = {};
  if (!values.reg_email) {
      errors.reg_email = strings("empty_email");
  } else if (!validator.isEmail(values.reg_email)) {
      errors.reg_email = strings("invalid_email");
  }
  if (!values.username) {
      errors.username = strings("empty_User");
  }
  if (!values.reg_phone) {
      errors.reg_phone = strings("phone_empty");
  }
  else if (values.reg_phone && !PHONE_REGEX.test(values.reg_phone)) {
      errors.reg_phone = strings("phone_invalid");
  }
  return errors;
};
const mapStateToProps = state => {
  console.log(state);
  const { email ,username,reg_phone,} = state.home;



  return {
    email,username,reg_phone,

  };
};

Home = connect(
  mapStateToProps,
  {
    employeeEmailChanged,
    employeeUsernameChanged,
    registerPhoneChanged,
    submitEmployeeDetails,
    registerDataClear
  }
)(Home);
export default reduxForm({ form: "home-data", validate })(Home);
