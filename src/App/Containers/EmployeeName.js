import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  Platform,
  FlatList,
  ImageBackground
} from "react-native";
import { connect } from 'react-redux';
import { strings } from "../../locales/i18n.js";
import { homeStyle } from "../../styles/homeStyle";
import { COLORS } from '../../Constants/colors';
import { SIZE_TEXT } from "../../Constants/dimension";

class EmployeeName extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    getEmployeeList = () =>{
        return (
            <FlatList 
            data={this.props.email}
            renderItem={({ item }) => 
            <TouchableOpacity  onPress={() => this.props.navigation.navigate(item.email, "EmployeeDetails")}
            >

              {item.email} 
            </TouchableOpacity>}
        />
        )
    }
  render(){
      return(
          <View style={homeStyle.mainContainer}>
            <View style={{ marginLeft:20 ,marginRight:20, marginTop: (Platform.OS === 'ios' ? -10 :'5%') }}>
            {this.getEmployeeList()}

                </View>
        </View>
      )
  }

}
  const mapStateToProps =(state)=>{
      const{email} = state.home
      return{
          email
      }
  }
EmployeeName = connect(
    mapStateToProps, {
})(EmployeeName);

export default EmployeeName