import {EMPLOYEE_EMAIL_CHANGED, EMPLOYEE_USERNAME_CHANGED,EMPLOYEE_PHONE_CHANGED,SUBMIT_DETAILS,SUBMIT_DETAILS_SUCCESS,SUBMIT_DETAILS_FAIL,REGISTER_CLEAR_DATA} from '../../Constants/action_type';
import {microApiHelper} from '../Config/microserviceApiHelper'


export const employeeEmailChanged = (email) => {
    return (dispatch) => {
        dispatch({ type: EMPLOYEE_EMAIL_CHANGED, payload: email });
    }
};

export const registerDataClear = ()=>
{
    return (dispatch) => {
        dispatch({ type: REGISTER_CLEAR_DATA });
    }  
}

export const employeeUsernameChanged = (username) => {
    return (dispatch) => {
        dispatch({ type: EMPLOYEE_USERNAME_CHANGED, payload: username });
    }
};

export const registerPhoneChanged = (phone) => {
    return (dispatch) => {
        dispatch({ type: EMPLOYEE_PHONE_CHANGED,payload:phone });
    }
};

export const submitEmployeeDetails=(body)=>{
    console.log(body , "body")
    return async (dispatch)=>{
        dispatch({ type:SUBMIT_DETAILS});
        const path = 'create';

            microApiHelper.post(path,body).then(successResponse=>{
                dispatch({type:SUBMIT_DETAILS_SUCCESS, payload:successResponse.data.result});
                    
            }).catch(error=>{
                dispatch({type:SUBMIT_DETAILS_FAIL});
            })
    } 

}