import { combineReducers } from "redux";
import { reducer as formReducer } from 'redux-form';
import homeReducers from '../Reducers/homeReducers';
import configStore from '../../stores/index'

const rootReducer = combineReducers({
   form: formReducer,
   home: homeReducers,
 
})
export default rootReducer;
