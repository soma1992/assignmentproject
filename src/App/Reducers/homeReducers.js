import { EMPLOYEE_EMAIL_CHANGED, EMPLOYEE_USERNAME_CHANGED ,EMPLOYEE_PHONE_CHANGED,SUBMIT_DETAILS,SUBMIT_DETAILS_SUCCESS,SUBMIT_DETAILS_FAIL,REGISTER_CLEAR_DATA} from '../../Constants/action_type';


const INITIAL_STATE = {
    email: '',
    username: '',
    reg_phone:'',
    success: false,
    error: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case REGISTER_CLEAR_DATA:
            return {...state,email:'',username:'', reg_phone:'',success:false,error:[],}
        case EMPLOYEE_EMAIL_CHANGED:
            return { ...state, email: action.payload, success: false, error: [] };
        case EMPLOYEE_USERNAME_CHANGED:
            return { ...state, username: action.paylaod, success: false, error: [] }
         case EMPLOYEE_PHONE_CHANGED:
            return { ...state, reg_phone: action.payload, error: [], success: false };
        case  SUBMIT_DETAILS:
            return{ ...state,success: false }
        case SUBMIT_DETAILS_SUCCESS:
            return { ...state, success: true }
        case SUBMIT_DETAILS_FAIL:
            return { ...state, success: false }
        default:
            return state
    }
}