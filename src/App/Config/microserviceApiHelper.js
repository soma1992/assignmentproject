import axios from 'axios';

 const microServiceOptions = {
    BASE_URL:'http://dummy.restapiexample.com/api/v1/', 
}
class microServiceApiHelper {

    get(relativeUrl) {
        return new Promise((resolve, reject) => {
         let headers = {
           "Content-Type": "application/json",
           "Cache-Control": "no-cache",
         };
    
         const URL = `${microServiceOptions.BASE_URL}${relativeUrl}`;
         axios({
           method: "get",
           url: URL,
           headers: headers,
         })
           .then(sucessRes => {
             resolve(sucessRes);
           }).catch(error => {
                       reject(error);
                     });
       });
     }

post(relativeUrl, body) {
    return new Promise((resolve, reject) => {
     let headers = {
       "Content-Type": "application/json",
       "Cache-Control": "no-cache",
     };

     const URL = `${microServiceOptions.BASE_URL}${relativeUrl}`;
     axios({
       method: "post",
       url: URL,
       data: body,
       headers: headers,
     })
       .then(sucessRes => {
         resolve(sucessRes);
       }).catch(error => {
                   reject(error);
                 });
   });
 }
 put(relativeUrl, body) {
    return new Promise((resolve, reject) => {
     let headers = {
       "Content-Type": "application/json",
       "Cache-Control": "no-cache",
     };

     const URL = `${microServiceOptions.BASE_URL}${relativeUrl}`;
     axios({
       method: "put",
       url: URL,
       data: body,
       headers: headers,
     })
       .then(sucessRes => {
         resolve(sucessRes);
       }).catch(error => {
                   reject(error);
                 });
   });
 }
 delete(relativeUrl, body) {
    return new Promise((resolve, reject) => {
     let headers = {
       "Content-Type": "application/json",
       "Cache-Control": "no-cache",
     };

     const URL = `${microServiceOptions.BASE_URL}${relativeUrl}`;
     axios({
       method: "delete",
       url: URL,
       data: body,
       headers: headers,
     })
       .then(sucessRes => {
         resolve(sucessRes);
       }).catch(error => {
                   reject(error);
                 });
   });
 }
}
 export const microApiHelper = new microServiceApiHelper();